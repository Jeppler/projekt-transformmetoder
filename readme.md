# A Guide to Signal Processing in Python
*By Jesper Carlsson, jesper.carlsson.8745@student.uu.se*

## Table of Contents
[TOC]

## Quick Start Guide
To get started quickly, download [the python file](transformmetoder.py) and change the values and transfer function in the ``main()`` function at the very bottom of the file. See [this section](#defining-an-lti-system) about how to input a transfer function. 

## Prerequisites
Besides Python, you will also need the librarys scipy, numpy and matplotlib. These can be installed by running ``pip install scipy numpy matplotlib`` or ``python3.10 -m pip install scipy numpy matplotlib`` to a specific version of python. 

Import these libraries into python: 
```python
from scipy import signal
from matplotlib import pyplot as plt
import numpy as np
```

## Defining an LTI-system
[Scipy docs](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.lti.html)

An LTI-system in scipy.signal can be defined with the system's transfer function. For a transfer function on the form
```math
    \frac{as + b}{cs^2 + ds + e},
```
the system is defined as: 
```python
lti = signal.lti([a, b], [c, d, e])
```
As seen in the example above, when the LTI-system is defined with a transfer function, it takes two arguments; Both are lists. The first list contains the polynomial coefficients in the descending order of exponents for the numerator and the second list is for the denominator. 

The filter for this project had the transfer function
```math
    \frac{1}{LC s^2 + (L/R)s + 1}
```
and is therefore defined in python as:
```python
R = 12.5 # Ohm
L = 25e-6 # Henry
C = 40e-9 # Farad

circuit = signal.lti([1], [L*C, L/R, 1])
```

Please note that you need to set values to your constants for some calculations. Symbolic variables will not yield the same result. 

## Step Response Function
[Scipy docs](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.lti.step.html)

Given a vector with a time interval, the step response function for that given time is:
```python
x = np.linspace(0, T_stop, resolution)
t, y = lti.step(T=x)
```
Here, $`x`$ is a numpy vector (almost list) that contains values from 0 to $`T_\textit{stop}`$ with a total of ``resolution`` number of points. This gives the time vector $`t`$ (same as $`x`$) and the $`y`$-values for every point in $`t`$. 

To superpose the analytical function, define the function as
```python
def step_response_function(t):
    return np.exp(-1e6*t)*(np.exp(1e6*t) - 1e6*t)
```
or
```python
step_response_function = lambda t: np.exp(-1e6*t)*(np.exp(1e6*t) - 1e6*t)
```
but with your specific function. Use the numpy vectorize function to create a version of function that can be applied to entire vectors instead of applying the function to each element in the list: 
```python
step_response_vectorized = np.vectorize(step_response_function)
```
The analytical function can now used and superposed:
```python
y_analytical = step_response_vectorized(t)

plt.plot(t, y, label='Scipy signal')
plt.plot(t, y_analyical, ':', label='Analytical')
plt.legend()
plt.show()
```

## Bode Plot
[Scipy docs](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.lti.bode.html)

To compute the Bode Plot for a frequency vector $`w`$, use:
```python
w = np.logspace(lower_freq_exp, higher_freq_exp, resolution)
w, mag, phase = lti.bode(w=w)
```
The method returns three values, a frequency vector $w$ in \[rad/s\], a magnitude vector \textit{mag} in \[dB\] and a phase vector \textit{phase} in \[deg\]. As Bode Plots have a logarithmic frqequency sclade, the inital vector $`\omega`$ should have a logarithmic distrubution. Otherwise the resolution will be unnecessarily high for high frequencies and insufficient for low frequencies. To get a logarthimic distrubution with 100 000 datapoints between $`10^3`$ and $`10^6`$ \[Hz\], use:
```python
w = np.logspace(3, 6, 100000) * 2 * np.pi
_, mag, phase = lti.bode(w=w)
mag = np.power(10, mag/20)
```

Please note that the output frequency vector is disregarded with the use of ``_``. If you wish to use the output frequency vector, divide it by $`2\pi`$ to get it in \[Hz\] instead of \[rad/s\]. The code above also includes a rescaling from \[dB\] to \[gain\]. 

### Dual Y-axis and Logarithmic Scale in Matplitlib
With the use of ``twinx()``, we can produce a plot with dual Y-axis. 
```python
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()

l1 = ax1.semilogx(w, mag, color='tab:blue', label='Magnitude')
l2 = ax2.semilogx(w, phase, color='orange', label='Phase shift')
lines = l1 + l2

ax1.set_xlabel('Frequency [Hz]')
ax1.set_ylabel('Magnitude [gain]')
ax2.set_ylabel('Phase shift [deg]')
self.ax.grid(True, which="both", ls='-')
self.ax2.grid()
self.fig.tight_layout()
fig.legend(lines, [l.get_label() for l in lines])
plt.show()
```

## Simulate Output
[Scipy docs](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.lti.output.html)

To simulate the output of the system, given a specific input signal ``in_signal``, use ``lti.output()``:
```python
x = np.linspace(0, T_stop, resolution)
in_signal = np.sin(freq*x)

t, out_signal = lti.output(in_signal, x)
```

## Fourier Series for Square Wave
To produce a square wave between with $`-1`$ and $`1`$,
```math
    \frac 4\pi \sum_{k=1}^n \frac{\sin((2k + 1)\omega x)}{2k+1}
```
with $`\omega = 2\pi/T_0`$, use
```python
x = np.linspace(0, T_stop, resolution)
signal = 4/np.pi*sum([np.sin(k*x*2*np.pi/period)/k for k in 2*np.arange(n)+1])
```
or
```python
x = np.linspace(0, T_stop, resolution)
signal = 0 * x

for k in range(n):
    signal += np.sin((2*k+1)*x*2*np.pi/period)/(2*k+1)

signal *= 4/np.pi
```