"""
Copyright (c) 2022 Jesper Carlsson
May be used for non-commercial academic purposes. Attribution to author required. 
"""

from scipy import signal
from matplotlib import pyplot as plt
import numpy as np
from typing import Tuple, Callable

class Plot:
    def __init__(self, title: str, xlabel: str, ylabel: str) -> None:
        self.fig, self.ax = plt.subplots()
        self.fig.suptitle(title)
        self.ax.set_xlabel(xlabel)
        self.ax.set_ylabel(ylabel)
        self.ax.grid()

    def add_line(self, data: Tuple[np.ndarray, np.ndarray], label: str, linestyle: str = '-'):
        self.ax.plot(*data, label=label, linestyle=linestyle)
    
    def show(self, legend_loc = None) -> None:
        self.ax.legend() if legend_loc is None else self.ax.legend(loc=legend_loc)
        plt.show()


class TwinxPlot(Plot):
    def __init__(self, title: str, xlabel: str, ylabel: str, second_label: str) -> None:
        super().__init__(title, xlabel, ylabel)
        self.ylabel = ylabel
        self.second_label = second_label
        self.ax2 = self.ax.twinx()

        self.ax.set_ylabel(ylabel, color='tab:blue')
        self.ax2.set_ylabel(second_label, color='orange')
    
    def set_data(self, x: np.ndarray, y1: np.ndarray, y2: np.ndarray) -> None:
        l1 = self.ax.semilogx(x, y1, color='tab:blue', label=self.ylabel)
        l2 = self.ax2.semilogx(x, y2, '--', color='orange', label=self.second_label)
        self.lines = l1 + l2

    def show(self):
        self.ax.grid(True, which="both", ls='-')
        self.ax2.grid()
        self.fig.tight_layout()
        self.ax.legend(self.lines, [l.get_label() for l in self.lines])
        plt.show()


class FilterCircuit:
    def __init__(self, numerator: list, denominator: list) -> None:
        self.lti = signal.lti(numerator, denominator)

    def plot_step_response(self, T: float, func: Callable, resolution: int = 100000) -> None:
        x = np.linspace(0, T, resolution)

        func = np.vectorize(func)

        plot = Plot('Step Response Function', 'Time [s]', 'Signal')
        plot.add_line(self.step_response(x), 'Scipy Signal')
        plot.add_line((x, func(x)), 'Analytic', linestyle='--')
        plot.show()

    def step_response(self, T: np.ndarray) -> Tuple[list, list]:
        return self.lti.step(T=T)

    def plot_bode(self, low_limit: float, high_limit: float, resolution: int = 100000) -> None:
        data = self.bode(np.logspace(low_limit, high_limit, resolution)*2*np.pi)

        plot = TwinxPlot('Bode Plot', 'Frequency [Hz]', 'Magnitude [gain]', 'Phase shift [deg]')
        plot.set_data(*data)
        plot.show()

    def bode(self, w: np.ndarray) -> Tuple[list, list, list]:
        w, mag, phase = self.lti.bode(w=w)
        return w/2/np.pi, np.exp(mag/20), phase
    
    def plot_simulate(self, freq: float, freqtxt: str, periods: float = 3, resolution: int = 100000) -> None:
        x = np.linspace(0, 2*np.pi*periods/freq, resolution)
        in_signal = np.sin(freq*x)

        in_signal, out_signal = self.simulate(x, in_signal)

        plot = Plot(f'Steady State Response {freqtxt}', 'Time [s]', 'Signal')
        plot.add_line(in_signal, 'In signal')
        plot.add_line(out_signal, 'Out signal', linestyle='--')
        plot.show(legend_loc='upper right')

    def simulate(self, x: np.ndarray, in_signal: np.ndarray) -> Tuple[Tuple[list, list], Tuple[list, list]]:
        T, out_signal, _ = self.lti.output(in_signal, x)

        return (x, in_signal), (T, out_signal)

    def plot_square_wave(self, period: float, k: int, length: float, resolution: int = 100000) -> None:
        x = np.linspace(0, length*period, resolution)
        sq_wave = self.fourier_square_wave(period, k, x)

        in_signal, out_signal = self.simulate(x, sq_wave)

        plot = Plot('Square Wave Simulation', 'Time [s]', 'Voltage [V]')
        plot.add_line(in_signal, 'In signal')
        plot.add_line(out_signal, 'Out signal', linestyle='--')
        plot.show(legend_loc='upper right')

    def fourier_square_wave(self, period: float, k: int, x: np.ndarray) -> np.ndarray:
        return sum([np.sin(n*x*2*np.pi/period)/n for n in 2*np.arange(k)+1])*2/np.pi
    
    def print_frequency_table(self, frequencies: list) -> None:
        data = self.bode(2*np.pi*np.array(frequencies))
        print('Frequency | Magnitude | Phase sift')
        print('----------+-----------+-----------')
        for w, mag, phase in zip(*data):
            print(f'{round(w):<9} | {round(mag, 2):<9} | {round(phase)}')


def main():
    # Defining constants
    R = 12.5 # Ohm
    L = 25e-6 # Henry
    C = 40e-9 # Farad

    # Creating filter circuit
    circuit = FilterCircuit([1], [L*C, L/R, 1])

    # Plotting step response function
    circuit.plot_step_response(1e-5, lambda t: 1 - np.exp(-1e6*t) - 1e6*t*np.exp(-1e6*t))

    # Plotting bode diagram
    circuit.plot_bode(3, 8)

    # Plotting the stady state response
    circuit.plot_simulate(1e3, '1 kHz')
    circuit.plot_simulate(1e6, '1 GHz')

    # Plotting the stady state response for square waves
    circuit.plot_square_wave(10e-6, 5, 3)

    # Print table of frequencies for sine wave
    circuit.print_frequency_table([1e5, 3e5, 5e5, 7e5, 9e5])


if __name__ == '__main__':
    main()